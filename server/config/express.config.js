var express = require('express');

var bodyParser = require('body-parser')

var routes = require('../login/login.routes');


module.exports = function(){

    var app = express();

    app.use( bodyParser.json() );       // to support JSON-encoded bodies
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
extended: true})); 
    routes(app);

    return app;

    
}
